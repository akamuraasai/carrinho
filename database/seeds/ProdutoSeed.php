<?php

use Illuminate\Database\Seeder;

class ProdutoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Produto::class, 40)->create()->each(function($p) {
            $p->categorias()->save(factory(App\Categoria::class)->make());
        });
    }
}

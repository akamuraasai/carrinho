<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Categoria::class, function (Faker\Generator $faker) {
    return [
        'nome' => $faker->words($faker->randomDigitNotNull, true),
    ];
});

$factory->define(App\Endereco::class, function (Faker\Generator $faker) {
    return [
        'logradouro' => $faker->streetName,
        'numero' => $faker->buildingNumber,
        'complemento' => $faker->secondaryAddress,
        'bairro' => $faker->cityPrefix,
        'cidade' => $faker->city,
        'estado' => $faker->state,
        'cep' => $faker->postcode,
    ];
});

$factory->define(App\Produto::class, function (Faker\Generator $faker) {
    return [
        'nome' => $faker->words($faker->randomDigitNotNull, true),
        'descricao' => $faker->sentences(2, true),
        'preco' => $faker->randomFloat(2, 15, 500),
        'imagem' => $faker->imageUrl(250, 200, 'cats'),
        'caracteristicas' => $faker->words($faker->randomDigitNotNull, true),
    ];
});

@extends('main')

@section('content')
        <div class="navbar navbar-inverse navbar-fixed-left">
            <a class="navbar-brand" href="#">Carrinho de Compras</a>
            <ul class="nav navbar-nav">
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" ng-click="limpaClickCategorias()">Categorias </a>
                <li ng-repeat="cat in categorias"><a href="#" ng-click="filtraCategoria(cat.id)">@{{ cat.nome }}</a></li>
            </ul>
        </div>
        <div class="container">
            <div class="col-xs-12 navbar navbar-default">
                <a class="navbar-brand pull-right" href="#"><i class="glyphicon glyphicon-shopping-cart"></i> Carrinho</a>
            </div>
            <h1>Produtos</h1>
            <input class="form-control col-xs-12" type="text" name="busca" id="busca" ng-model="busca_op" ng-keyup="buscar()">

            <div class="col-xs-12" ng-init="listar()">
                <center>
                    <div class="col-xs-3 panel" ng-repeat="item in itens" style="padding: 20px;">
                        <div class="panel-body" style="min-height: 310px;">
                            <a href="/produto/@{{ item.id }}">
                                <img src="@{{ item.imagem }}" class="col-xs-12">
                                <label class="col-xs-12 control-label">@{{ item.nome }}</label>
                                <h4 class="col-xs-12">R$ @{{ item.preco }}</h4>

                            </a>
                        </div>
                        <button class="btn btn-success col-xs-12" ng-click="comprar(item.id)">Comprar</button>
                    </div>
                </center>
            </div>
        </div>

@stop

@section('js-especifico')
    <script src="/js/index.js"></script>
@stop
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $fillable = [
      'logradouro', 'numero', 'complemento',
      'bairro', 'cidade', 'estado', 'cep',
    ];
}

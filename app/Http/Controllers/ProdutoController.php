<?php

namespace App\Http\Controllers;

use App\Produto;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProdutoController extends Controller
{
    public function lista()
    {
        return Produto::with('categorias')->get();
    }
}

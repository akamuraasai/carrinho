<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    //
    protected $fillable = [
      'nome', 'descricao', 'preco',
      'imagem', 'caracteristicas',
    ];

    public function categorias()
    {
        return $this->belongsToMany('App\Categoria');
    }
}

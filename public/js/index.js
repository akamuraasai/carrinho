var app = angular.module('exampleApp', []);
app.controller('exampleCtrl', function($scope, $http) {
    $scope.item = {};
    $scope.itens = [];
    $scope.itens_inicial = [];
    $scope.busca_op = '';
    $scope.categorias = [];

    function filtraItens(i, p, s) {
        return i.nome.indexOf(this) >= 0 ? true : false;
    }

    function filtraItensCategoria(i, p, s) {
        return i.categorias[0].id == this;
    }

    $scope.listar = function () {
        $http.get("/produtos")
            .then(function(resposta) {
                $scope.itens_inicial = resposta.data;
                $scope.itens = resposta.data;
            });

        $http.get("/categorias")
            .then(function(resposta) {
                $scope.categorias = resposta.data;
            });
    }

    $scope.limpaClickCategorias = function () {
        $scope.itens = $scope.itens_inicial;
    }

    $scope.buscar = function () {
        $scope.itens = $scope.itens_inicial.filter(filtraItens, $scope.busca_op);
    }

    $scope.filtraCategoria = function (id) {
        $scope.itens = $scope.itens_inicial.filter(filtraItensCategoria, id);
    }
});